Laravel 5.7 requirements

PHP >= 7.1.3  
OpenSSL PHP Extension  
PDO PHP Extension  
Mbstring PHP Extension  
Tokenizer PHP Extension  
XML PHP Extension  
Ctype PHP Extension  
JSON PHP Extension  

Laravel utilizes [Composer](https://getcomposer.org/) to manage its dependencies. So, before using Laravel, make sure you have Composer installed on your machine.

Clone project using following command

`git clone https://asadzaheer@bitbucket.org/asadzaheer/singularity-creations-json-validation-task.git`

Go to project directory rename .env.example file to .env and run following command to install laravel dependencies.

`composer install`

once all the dependencies are installed run following commands

`php artisan key:generate`

`php artisan serve`

Laravel development server will be started on <http://127.0.0.1:8000>

