<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 35px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="title m-b-md">
            Singularity Json File Validator
        </div>

        <div class="links">
            <form method="POST" action="/fileHandler" enctype="multipart/form-data">
                @csrf
                <input type="file" name="json" required/>
                <input type="submit" class="btn" name="Upload">
            </form>
            @if(session('result'))
            <h4>Results</h4>
            <ul>
                @foreach(session('result') as $key=>$value)
                <li>
                    @if(is_array($value))
                    {{$key}} : {{implode(',',$value)}}
                    @else
                    {{$key}} : {{$value}}
                    @endif
                </li>
                @endforeach
            </ul>
            @endif
            @if(session('results'))
            <h4>Results</h4>
            @foreach(session('results') as $array)
            <ul>
                @foreach($array as $key=>$value)
                <li>
                    @if(is_array($value))
                    {{$key}} : {{implode(',',$value)}}
                    @else
                    {{$key}} : {{$value}}
                    @endif
                </li>
                @endforeach
            </ul>
            @endforeach
            @endif
        </div>
    </div>
</div>
</body>
</html>
