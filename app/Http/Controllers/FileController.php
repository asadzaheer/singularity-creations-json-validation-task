<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class FileController extends Controller {
    function fileHandler(Request $request) {
        $json = $request->file('json');
        if ($json->getClientMimeType() !== 'application/json') {
            return redirect('/')->with('result', ['error' => 'File is not a json type.']);
        } else {
            $jsonContent = json_decode(file_get_contents($json), true);

            // Check if multidimensional array
            if ($this->is_multiArray($jsonContent)) {
                $results = [];
                foreach ($jsonContent as $array) {
                    $result = $this->validate_json_content($array);
                    array_push($results, $result);
                }
                return redirect('/')->with('results', $results);
            } else {
                $result = $this->validate_json_content($jsonContent);
                return redirect('/')->with('result', $result);
            }
        }
    }

    /**
     * Validate json file ccontents
     * @param $json
     * @return array
     */
    function validate_json_content($json) {
        $checkKeys = $this->array_keys_exist($json, ['name', 'phone', 'address', 'website']);

        // If all keys exists and have non-empty value
        if ($checkKeys['status'] === true) {
            // Validate phone number
            if (preg_match('/^\(?([0-9]+)\)?[- ]?([0-9]+)[- ]?([0-9]+)$/', $json['phone']) !== 1) {
                return ['error' => 'Invalid phone number.', 'phone' => $json['phone']];
            }
            if (preg_match('/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/', $json['website']) !== 1) {
                return ['error' => 'Invalid website url.', 'website' => $json['website']];
            }
        } else {
            return ['error' => 'Following keys are missing or empty.', 'key(s)' => $checkKeys['missingKeys']];
        }

        return $json;
    }

    /**
     * Check if array is multidimensional
     *
     * @param array $arr
     *
     * @return bool
     */
    function is_multiArray($arr) {
        rsort($arr);
        return isset($arr[0]) && is_array($arr[0]);
    }

    /**
     * Checks if multiple keys exist in an array
     *
     * @param array $array
     * @param array|string $keys
     *
     * @return array
     */
    function array_keys_exist(array $array, $keys) {
        $count       = 0;
        $missingKeys = [];

        foreach ($keys as $key) {
            if (isset($array[$key]) || array_key_exists($key, $array)) {
                if ($array[$key] !== '') {
                    $count++;
                } else {
                    array_push($missingKeys, $key);
                }
            } else {
                array_push($missingKeys, $key);
            }
        }

        return ['status' => count($keys) === $count, 'missingKeys' => $missingKeys];
    }
}
